﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DockerTest.NUnit.Test
{
    [TestFixture]
    public class CalculationNunitTest
    {

        [Test]
        public void SumShouldRetureCorrectValue()
        {
            Calculation obj = new Calculation();
            int res= obj.Sum(12, 2);
            Assert.IsTrue(res == 14);
        }
    }
}
