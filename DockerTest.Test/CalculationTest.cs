﻿using System;
using DockerTest;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DocerTes.Test
{
    [TestClass]
    public class CalculationTest
    {
        [TestMethod]
         public void SumMethodShouldReturnCorrectValue()
        {
            Calculation obj = new Calculation();
            int res = obj.Sum(1, 5);
            Assert.IsTrue(res==6);

           
        }
    }
}
